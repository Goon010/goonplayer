const remote = require('electron').remote;
const ipcRenderer = require('electron').ipcRenderer;
//const app = require('electron').remote.app;
const jQuery = require('jquery');
const $ = jQuery;
//const settings = require('electron-settings');
require('jquery-ui-dist/jquery-ui');
const Mousetrap = require('mousetrap');
const internetradio = require('node-internet-radio');
const Request = require('request');
const Swal = require('sweetalert2');
const URL = require('url').URL;

//shutup the event emitter warning
require('events').EventEmitter.prototype._maxListeners = 100;

let playing = false;
const lastFMApiKey = 'afda6c545066128fe00bb149ef71fa11';
let audioSrc = "http://1a-rock.radionetz.de/1a-rock.mp3";
let stationName = "Goon's Tunes";
let audio, context, analyser, src = null;
let metadataFetchInterval = null;

let appname, appversion = "";

ipcRenderer.on('appdetails', (e, v)=>{
    appname = v.name;
    appversion = v.version;

    jQuery(() => {

        audio = window.document.createElement('audio');
        audio.id = 'audio';
        $('.wrapper').append(audio);
      
        $(".minimize").on('click',() => {
          ipcRenderer.send('minimize');
        });
        $(".exit").on('click',() => {
          ipcRenderer.send('exit');
        });
      
        $(".title").text(appname);
        $(".version-label").text('v' + appversion);
      
        $(".playpause-btn").on('click',() => {
          startAudio(audioSrc);
        });
      
        $("#volume").slider({
          min: 0,
          max: 50,
          value: 10, //settings.getSync('volume', 10),
          range: "min",
          slide: function (event, ui) {
            setVolume(ui.value / 100);
            //settings.setSync('volume', ui.value);
          }
        });
        setVolume(/*settings.getSync('volume', 10)*/ 10 / 100);
      
        $("#audio").on('loadeddata', () => {
          if (playing) {
            $(".playpause-btn i")
              .removeClass('fa-spinner')
              .removeClass('spin')
              .addClass('fa-stop');
          }
        });
      
        context = new AudioContext();
        src = context.createMediaElementSource($("#audio")[0]);
        analyser = context.createAnalyser();
        visualizerInit();
      
        Mousetrap.bind('ctrl+o', async () => {
          const {
            value: r
          } = await Swal.fire({
            title: 'Open URL',
            input: 'url',
            inputPlaceholder: 'Enter URL'
          });
      
          if (r) {
            if (playing) {
              startAudio(audioSrc);
              audioSrc = r;
              startAudio(audioSrc);
            } else {
              audioSrc = r;
              startAudio(audioSrc);
            }
          }
        });
        Mousetrap.bind('ctrl+b', openBookmarks);
        $('.bookmark').on('click', openBookmarks);
      });
});

ipcRenderer.send('appdetails');
process.setMaxListeners(0);

function startAudio(src) {
  $(".title").text(appname);

  if (!playing) {
    $(".playpause-btn i")
      .removeClass('fa-play')
      .addClass('fa-spinner')
      .addClass('spin')
      .prop("disabled", true);

    playing = true;
    getStationTitle(audioSrc);

    //settings.set('last_url', audioSrc);

    $("#audio")[0].src = audioSrc;
    $("#audio")[0].play();

    metadataFetchInterval = setInterval(() => {
      getMetadata($("#audio")[0].src);
    }, 3000);

    getMetadata($("#audio")[0].src);

  } else {
    $(".playpause-btn i")
      .removeClass('fa-spinner')
      .addClass('fa-play')
      .removeClass('spin')
      .removeClass('fa-stop')
      .prop("disabled", false);

    playing = false;

    $("#audio")[0].audioSrc = '';
    $("#audio")[0].load();

    clearInterval(metadataFetchInterval);

    $(".song").text('');
    $('.artist').text('');
    clearAlbumArt();
  }
}

function getStationTitle(url) {
    url = new URL(url);
    var protocol = (url.protocol == "http:") ? require('http') : require('https');
    var req = protocol.get(url.toString(), (res) => {
        console.log('statusCode:', res.statusCode);
        console.log('headers:', res.headers);

        res.on('data', (d) => {
            stationName = res.headers['icy-name'];
            $(".title").text(appname + ' - ' + stationName);
            req.destroy();
        });
    }).on('error', (e) => {
        console.error(e);
    });
}

function setVolume(volume) {
  if(isNaN(volume))
    volume = 0.5;
  var audio = $("audio")[0];
  audio.volume = volume;
}

function visualizerInit() {
  var canvas = document.getElementById("canvas");
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var ctx = canvas.getContext("2d");

  src.connect(analyser);
  analyser.connect(context.destination);

  analyser.fftSize = 256;
  analyser.smoothingTimeConstant = 0.85;

  var bufferLength = analyser.frequencyBinCount;
  //console.log(bufferLength);

  var dataArray = new Uint8Array(bufferLength);

  var WIDTH = canvas.width;
  var HEIGHT = canvas.height;

  var barWidth = (WIDTH / bufferLength) * 2.4;
  var barHeight;
  var x = 0;

  function renderFrame() {
    requestAnimationFrame(renderFrame);

    x = 0;

    analyser.getByteFrequencyData(dataArray);

    ctx.fillStyle = "#24292e";
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    for (var i = 0; i < bufferLength; i++) {
      barHeight = dataArray[i];

      var b = barHeight + (15 * (i / bufferLength));
      var g = 250 * (i / bufferLength);
      var r = 70;

      ctx.fillStyle = "rgb(" + r + "," + g + "," + b + ")";
      ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

      x += barWidth + 1;
    }
  }
  renderFrame();
}

function getMetadata(url) {
  if (url == '') return;
  try{
    internetradio.getStationInfo(url, function (error, station) {
      if(error){ 
        //console.log(error);
        return;
      }
      if (!playing) return;
      if (typeof (station) !== 'undefined') {
        let title = station.title;
        let artist = '';
        let song = '';
        if (title.indexOf(' - ') !== -1) {
          let split = title.split(' - ');
          artist = split[0].trim();
          song = split[1].trim();
        } else
          song = title;

        if(song.match(/text=/i)){
          song = song.match(/text="(.*)" song/i)[1];
        }

        if ((song !== '' && artist !== '') &&
          ($(".song").text() !== song || $(".artist").text() !== artist))
          albumArtFetch(artist, song);

        if (song == '' && artist !== '')
          clearAlbumArt();

        if ($(".song").text() !== song) {
          $(".song").text(song);
          $('.artist').text(artist);
          console.log(title, 'is playing');
        }
      }
    });
  }catch(e){
    //console.log(e);
  }
}

function albumArtFetch(artist, song) {
  let url = `http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=${lastFMApiKey}&artist=${encodeURIComponent(artist)}&track=${encodeURIComponent(song)}&format=json`;
    
  let resp = $.getJSON(url);

  resp.done(() => {
    if (!playing) return;
    if (typeof (resp.responseJSON) !== 'undefined') {
      let data = resp.responseJSON;

      if (typeof (data.track) !== 'undefined' && typeof (data.track.album) !== 'undefined' && typeof (data.track.album.image) !== 'undefined') {
        let albumArtURL = data.track.album.image.pop()['#text'];
        console.log('Found album art!! [' + url + ']');
        console.log(albumArtURL);

        if (albumArtURL !== "") {
          var canvas = document.getElementById('albumart');
          var ctx = canvas.getContext('2d');
          var img = new Image();
          img.onload = function () {

            ctx.imageSmoothingEnabled = false;
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, img.width, img.height);

            $("#albumart").addClass('active-border').attr("style", '');

            ipcRenderer.send('thumbnailset');
          };
          img.src = albumArtURL;
        } else
          clearAlbumArt();
      } else {
        clearAlbumArt();
        console.log('No album art :( [' + url + ']');
      }
    } else {
      clearAlbumArt();
      console.log(resp.responseJSON);
    }
  });
}

function clearAlbumArt() {
  $("#albumart").removeClass('active-border').attr("style", 'border: none');

  var canvas = document.getElementById('albumart');
  var ctx = canvas.getContext('2d');

  ctx.clearRect(0, 0, canvas.width, canvas.height);

  ipcRenderer.send('clearthumbnail');
}

async function openBookmarks()
{
  options = {};
  var defaultBokmarks = require('./default-bookmarks');
  for (var i = 0; i < defaultBokmarks.length; i++) {
    options[i] = defaultBokmarks[i].name;
  }

  const {
    value: station
  } = await Swal.fire({
    title: 'Load Bookmark',
    input: 'select',
    inputOptions: options,
    inputPlaceholder: 'Select a station',
    showCancelButton: true
  });

  if (station) {
    if (playing) {
      startAudio(audioSrc);
      audioSrc = defaultBokmarks[station].url;
      startAudio(audioSrc);
    } else {
      audioSrc = defaultBokmarks[station].url;
      startAudio(audioSrc);
    }
    stationName = defaultBokmarks[station].name;
  }
}