const {
    app,
    BrowserWindow,
    ipcMain
} = require('electron');
let updater = require('./updater');
let config = require('./package.json');

let mainWindow;

app.on('ready', init);
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
app.on('activate', () => {
    if (mainWindow === null) {
        init();
    }
});

ipcMain.on('exit',(event,args) => {
    app.exit();
});
ipcMain.on('minimize', (e, a) => {
    BrowserWindow.getFocusedWindow().minimize();
});
ipcMain.on('appdetails', (e, a) => {
    mainWindow.webContents.send('appdetails', {name: app.name, version: app.getVersion()});
});
ipcMain.on('thumbnailset', (e,a) => {
    console.log('set thumbnail');
    BrowserWindow.getAllWindows()[0].setThumbnailClip({
        x: 58,
        y: 90,
        width: 300,
        height: 300
    });
});
ipcMain.on('clearthumbnail', (e,a) => {
    console.log('cleared thumbnail');
    BrowserWindow.getAllWindows()[0].setThumbnailClip({
        x: 0,
        y: 0,
        width: 0,
        height: 0
    });
});

function init() {
    mainWindow = new BrowserWindow({
        title: 'Goon Player v' + config.version + " " + config.version_label,
        minWidth: 643,
        minHeight: 484,
        transparent: true,
        frame: false,
        maximizable: false,
        fullscreenable: false,
        resizable: false,
        webPreferences: {
            enableRemoteModule: true,
            contextIsolation: false,
            webSecurity: false,
            nodeIntegration: true,
            devTools: (process.argv.length > 2 && (process.argv[2] === "--inspect" || process.argv[2] === "-d")) ? true : false
        },
        show: false
    });
    mainWindow.loadFile('index.html');

    // Open the DevTools.
    if (process.argv.length > 2 && (process.argv[2] === "--inspect" || process.argv[2] === "-d"))
        mainWindow.webContents.openDevTools({
            mode: 'detach'
        });
    else
        updater.checkForUpdates();

    mainWindow.on('closed', () => {
        mainWindow = null;
    });
    mainWindow.on('ready-to-show', () => {
        mainWindow.show();
    });
}