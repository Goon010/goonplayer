/**
 * updater.js
 *
 * Please use manual update only when it is really required, otherwise please use recommended non-intrusive auto update.
 *
 * Import steps:
 * 1. create `updater.js` for the code snippet
 * 2. require `updater.js` for menu implementation, and set `checkForUpdates` callback from `updater` for the click property of `Check Updates...` MenuItem.
 */
const { dialog } = require('electron');
const { autoUpdater } = require('electron-updater');

autoUpdater.autoDownload = false;

autoUpdater.on('error', (error) => {
  //dialog.showErrorBox('Error: ', error == null ? "unknown" : (error.stack || error).toString())
});

autoUpdater.on('update-available', (info) => {
  setTimeout(() => {
    dialog.showMessageBox({
      type: 'info',
      title: 'Update',
      message: 'A new version was released, do you want update now?' +
       (info.releaseNotes.length > 0 ? '\nRelease Notes:\n'+info.releaseNotes:''),
      buttons: ['Sure', 'No']
    }, (buttonIndex) => {
      if (buttonIndex === 0) {
        autoUpdater.downloadUpdate();
      }
    });
  }, 2000);
});

autoUpdater.on('update-not-available', () => {
  // dialog.showMessageBox({
  //   title: 'No Updates',
  //   message: 'Current version is up-to-date.'
  // })
});

autoUpdater.on('update-downloaded', () => {
  dialog.showMessageBox({
    title: 'Install Updates',
    message: 'Updates downloaded, application will quit for the update...'
  }, () => {
    setImmediate(() => autoUpdater.quitAndInstall());
  });
});

// export this to MenuItem click callback
function checkForUpdates (menuItem, focusedWindow, event) {
  // updater = menuItem
  // updater.enabled = false
  console.log('Checking for updates...');
  autoUpdater.checkForUpdates();
}
module.exports.checkForUpdates = checkForUpdates;